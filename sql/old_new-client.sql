create procedure old_new_client (date_1 date, date_2 date)
returns
  (
  client_cnt integer,
  client_type varchar(20)
  )
as
begin
  for
    select count(distinct co.ID_CLIENT) as client_cnt, 'old_clients' as client_type
    from CLIENT_ORDER co
      where
      co.ID_CLIENT in (select co.id_client from CLIENT_ORDER co where co.ORDER_DATE<:date_1)
      and co.ORDER_DATE between :date_1 and :date_2

    union
    select count(distinct co.ID_CLIENT) as client_cnt, 'new_clients' as client_type
    from CLIENT_ORDER co
      where
      co.ID_CLIENT not in (select co.id_client from CLIENT_ORDER co where co.ORDER_DATE<:date_1)
      and co.ORDER_DATE between :date_1 and :date_2
    into :client_cnt, :client_type
  do
  suspend;
end
