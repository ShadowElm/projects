-- Делаем выборку всех записей из таблицы
create view Game_s as select * from GAME;
create view Client_s as select * from CLIENT;
create view CLIENT_ORDER_S as select * from CLIENT_ORDER;
create view COUNTRY_S as select * from COUNTRY;
create view CREATOR_S as select * from CREATOR;
create view GAME_PICTURE_S as select * from GAME_PICTURE;
create view ORDER_DETAIL_S as select * from ORDER_DETAIL;
create view VENDOR_S as select * from VENDOR;
create view VENDOR_CONTRACT_S as select * from VENDOR_CONTRACT;
create view CONTRACT_DETAIL_S as select * from CONTRACT_DETAIL;

-- Выборка игр, в которую может играть 4 игрока
create view Player_4 as select name, number_of_players from GAME where
 number_of_players = 4;

-- Выборка игр, рейтинг которых равен 4 или 6,5 баллам
 create view Raiting_1 as select g.name, g.RATING from GAME g where g.RATING in (4.00,6.50);

-- Выборка игр, рейтинг которых лежит в пределах от 4 до 7 баллов
 create view Raiting_2 as select g.name, g.RATING from GAME g where g.RATING between 4.00 and 7.00;

-- Общее количество заказанных игр
create view game_cnt_s as select sum(od.GAME_CNT) as games_cnt from order_detail od;

-- Выбираем все заказы, сортируем по дате, номеру и сумме заказа
create view order_date_s as select * from CLIENT_ORDER od order by od.ORDER_DATE desc, od.ORDER_PRICE desc, od.ORDER_NUM;

-- Выборка всех создателей с именами игр, которые они создали, и именем производителя данной игры
create view create_s as select ct.FIRST_NAME, ct.MIDDLE_NAME, ct.LAST_NAME, g.NAME from Creator ct,
 game g where g.ID_CREATOR=ct.ID;

-- Выборка всех клиентов, которые сделали заказ на сумму в пределах от 2 до 3,5 тысяч
create view cl_order_s as select cl.first_name, cl.middle_name, cl.last_name, co.order_price from CLIENT cl,
CLIENT_ORDER co where (co.order_price between 2000 and 3500) and (co.ID_CLIENT=cl.ID);


-- Создайте запрос, вычисляющий несколько совокупных характеристик таблиц
create view max_avg_s as select max(g.RATING) as raiting, avg(g.PRICE) as price from game g;


--Создайте запрос, рассчитывающий совокупную характеристику с использованием группировки,
-- наложите ограничение на результат группировки
create view g_cnt_s as select od.id_game as game_id, sum(od.GAME_CNT) as g_cnt from order_detail od group by od.ID_GAME;


--Придумайте и реализуйте пример использования вложенного запроса
create view use_vendor as select v.id, v.NAME from VENDOR v where id in
(select vc.ID_VENDOR from VENDOR_CONTRACT vc);


-- Добавим запись в таблицу клиентов
create procedure insert_client (id integer, id_country integer, FIRST_NAME varchar(50),
 MIDDLE_NAME varchar(50), LAST_NAME varchar(50), E_MAIL varchar(100), ADRESS varchar(100), PHONE_NUMBER varchar(20))
as
begin
    insert into CLIENT (ID, ID_COUNTRY, FIRST_NAME, MIDDLE_NAME, LAST_NAME, E_MAIL, ADRESS, PHONE_NUMBER)
    values (:ID, :ID_COUNTRY, :FIRST_NAME, :MIDDLE_NAME, :LAST_NAME, :E_MAIL, :ADRESS, :PHONE_NUMBER);
end;

-- Добавим запись в таблицу заказов
create procedure insert_client_order (ID integer, ID_CLIENT integer, ORDER_NUM varchar(20),
 ORDER_DATE date, ORDER_PRICE numeric(6,2), IS_NEED_DELIVERY char(1), DELIVERY_DATE date,
  IS_PAID char (1), PAID_DATE date)
as
begin
insert into CLIENT_ORDER (ID, ID_CLIENT, ORDER_NUM, ORDER_DATE, ORDER_PRICE, IS_NEED_DELIVERY, DELIVERY_DATE, IS_PAID,
                          PAID_DATE)
values (:ID, :ID_CLIENT, :ORDER_NUM, :ORDER_DATE, :ORDER_PRICE, :IS_NEED_DELIVERY, :DELIVERY_DATE, :IS_PAID, :PAID_DATE);
end;

-- Добавим запись в таблицу деталей контрактов
create procedure insert_contract_detail (ID integer, ID_VENDOR_CONTRACT integer,
 ID_GAME integer, GAME_PRICE numeric(6,2), GAME_CNT integer)
as
begin
 insert into CONTRACT_DETAIL (ID, ID_VENDOR_CONTRACT, ID_GAME, GAME_PRICE, GAME_CNT)
 values (:ID, :ID_VENDOR_CONTRACT, :ID_GAME, :GAME_PRICE, :GAME_CNT);
end;

-- Добавим запись в таблицу стран
create procedure insert_country (ID integer, CODE_2 varchar(2), NAME varchar(250))
as
begin
 insert into COUNTRY (ID, CODE_2, NAME)
 values (:ID, :CODE_2, :NAME);
end;

-- Добавим запись в таблицу создателей
create procedure insert_creator (ID integer, FIRST_NAME varchar(50),
 MIDDLE_NAME varchar(50), LAST_NAME varchar(50))
as
begin
  insert into CREATOR (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME)
  values (:ID, :FIRST_NAME, :MIDDLE_NAME, :LAST_NAME);
end;

-- Добавим запись в таблицы
create procedure insert_game (ID integer, ID_CREATOR integer, ID_COUNTRY integer,
 NAME varchar(100), NUMBER_OF_PLAYERS integer, PRICE numeric (6,2), RATING numeric (6,2),
  GAMES_IN_STOCK integer, RESERVED_CNT integer)
as
begin
 insert into GAME (ID, ID_CREATOR, ID_COUNTRY, NAME, NUMBER_OF_PLAYERS, PRICE, RATING, GAMES_IN_STOCK, RESERVED_CNT)
 values (:ID, :ID_CREATOR, :ID_COUNTRY, :NAME, :NUMBER_OF_PLAYERS, :PRICE, :RATING, :GAMES_IN_STOCK, :RESERVED_CNT);
end;

create procedure insert_order_detail (ID integer, ID_CLIENT_ORDER integer,
 ID_GAME integer, GAME_PRICE numeric(6,2), GAME_CNT integer)
as
begin
  insert into ORDER_DETAIL (ID, ID_CLIENT_ORDER, ID_GAME, GAME_PRICE, GAME_CNT)
  values (:ID, :ID_CLIENT_ORDER, :ID_GAME, :GAME_PRICE, :GAME_CNT) ;
end;

create procedure insert_vendor(ID integer, ID_COUNTRY integer, NAME varchar(100),
 ADRESS varchar(100), PHONE_NUMBER varchar(20))
as
begin
insert into VENDOR (ID, ID_COUNTRY, NAME, ADRESS, PHONE_NUMBER)
values (:ID, :ID_COUNTRY, :NAME, :ADRESS, :PHONE_NUMBER)  ;
end;

create procedure insert_vendor_contract (ID integer, ID_VENDOR integer,
 CONTRACT_NUM varchar(20), CONTRACT_DATE date, CONTRACT_PRICE numeric(6,2),
  IS_PAID char(1), PAID_DATE date,IS_RECEIVED char(1), DELIVERY_DATE date)
as
begin
 insert into VENDOR_CONTRACT (ID, ID_VENDOR, CONTRACT_NUM, CONTRACT_DATE, CONTRACT_PRICE, IS_PAID, PAID_DATE,
                              IS_RECEIVED, DELIVERY_DATE)
 values (:ID, :ID_VENDOR, :CONTRACT_NUM, :CONTRACT_DATE, :CONTRACT_PRICE, :IS_PAID, :PAID_DATE, :IS_RECEIVED,
         :DELIVERY_DATE) ;
end;

-- Выведем пять наиболее популярных игр
create view TOP5 as
select
  game.name as name,
  count(order_detail.id) as buyed
from
  GAME,
  order_detail
where
  game.id = order_detail.ID_GAME
group by
--  game.id,
  game.name
order by
  buyed desc;

-- Выведем рейтинг вендоров по объемам продаж их игр
create view top_vendor as
select
  v.NAME,
  sum(cd.GAME_CNT) as cnt
from
  VENDOR v,
  VENDOR_CONTRACT vc,
  CONTRACT_DETAIL cd
where
  (v.id=vc.ID_VENDOR) and
  (vc.ID=cd.ID_VENDOR_CONTRACT)
group by
  v.name
order by
 cnt desc;

