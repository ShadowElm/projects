create or alter procedure SALES_DYNAMIC (REP_YEAR integer, id_creator integer)
returns (
    ORDER_QUARTER integer,
    SUM_PREV_YEAR numeric(6,2),
    CNT_PREV_YEAR integer,
    SUM_CURR_YEAR numeric(6,2),
    CNT_CURR_YEAR integer)
AS
begin
  for
    with v_detailed_data as
      (
      -- ���������� ���
      select
        co.ORDER_DATE,
        od.GAME_PRICE as price_prev_year, od.GAME_CNT as cnt_prev_year,
        0 as price_curr_year, 0 as cnt_curr_year
      from order_detail od, creator ct, game g, client_order co
      where ct.id = g.ID_CREATOR and g.id = od.ID_GAME and co.ID = od.ID_CLIENT_ORDER
      and extract(YEAR from co.ORDER_DATE) = :rep_year-1
    
      union all
      -- ������� ���
      select
        co.ORDER_DATE,
        0 as price_prev_year, 0 as cnt_prev_year,
        od.GAME_PRICE as price_curr_year, od.GAME_CNT as cnt_curr_year
      from order_detail od, creator ct, game g, client_order co
      where ct.id = g.ID_CREATOR and g.id = od.ID_GAME and co.ID = od.ID_CLIENT_ORDER
      and extract(YEAR from co.ORDER_DATE) = :rep_year
      )
    --
    select
      (extract(month from order_date) + 2) / 3 as order_quarter,
      sum(price_prev_year) as price_prev_year,
      sum(cnt_prev_year) as cnt_prev_year,
      sum(price_curr_year) as price_curr_year,
      sum(cnt_curr_year) as cnt_curr_year
    from v_detailed_data
    group by order_quarter -- (extract(month from order_date) + 2) / 3
    order by order_quarter
    into
      :order_quarter,
      :sum_prev_year,
      :cnt_prev_year,
      :sum_curr_year,
      :cnt_curr_year
  do
    suspend;
end

suspend;

end
