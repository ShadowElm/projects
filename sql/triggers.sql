CREATE OR ALTER TRIGGER BU_CLIENT_ORDER FOR CLIENT_ORDER
ACTIVE BEFORE UPDATE POSITION 0
as
declare variable v_is_free_delivery char(1);
begin
  execute procedure CHECK_FREE_DELIVERY(new.ORDER_PRICE) returning_values (:v_is_free_delivery);

  if (:v_is_free_delivery='1') then new.is_free_delivery='1';
  else new.is_free_delivery='0';
end

CREATE OR ALTER TRIGGER BI_ORDER_DETAIL FOR ORDER_DETAIL
ACTIVE BEFORE INSERT POSITION 0
as
declare variable cnt integer;
begin
  if (new.id is null) then
    new.id = gen_id(gen_order_detail, 1);

  if (new.game_price is null) then new.game_price = 0;

  select count(od.ID) as CNT
  from order_detail od
  where
    od.ID_CLIENT_ORDER = new.ID_CLIENT_ORDER
    and od.ID_GAME=new.ID_GAME
  into cnt;

  if (cnt>0) then
    exception  e_game_already_in_order;

end

