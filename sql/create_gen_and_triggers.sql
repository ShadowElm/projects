create generator gen_creator;
create generator gen_country;
create generator gen_client;
create generator gen_game;
create generator gen_client_order;
create generator gen_game_picture;
create generator gen_order_detail;


create or alter trigger bi_creator for creator
active before insert position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_creator, 1);
end;


create or alter trigger bi_country for country
active before insert position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_country, 1);
end;


create or alter trigger bi_client for client
active before insert position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_client, 1);
end;

create or alter trigger bi_game for game
active before insert position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_game, 1);
end ;

create or alter trigger bi_game_picture for game_picture
active before insert position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_game_picture, 1);
end;

create or alter trigger bi_client_order for client_order
active before insert position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_client_order, 1);
end;


create or alter trigger bi_order_detail for order_detail
active before insert position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_order_detail, 1);

  update GAME set reserved_cnt = reserved_cnt + new.game_cnt where game.id = new.id_game;
end;

create or alter trigger ai_order_detail for order_detail
active after insert position 0
as
begin
  update CLIENT_ORDER co
  set co.ORDER_PRICE = coalesce(
    (select sum(od.game_price)
    from ORDER_DETAIL od where od.id_client_order=co.id), 0)
  where co.ID = new.id_client_order;
end;


create or alter trigger au_order_detail for order_detail
active after update position 0
as
begin
  update CLIENT_ORDER co
  set co.ORDER_PRICE = coalesce(
    (select sum(od.game_price)
    from ORDER_DETAIL od where od.id_client_order=co.id), 0)
  where co.ID = new.id_client_order;
end;


create generator gen_vendor;
create generator gen_vendor_contract;
create generator gen_contract_detail;

create or alter trigger bi_vendor for vendor
active before insert position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_vendor, 1);
end;


create or alter trigger bi_vendor_contract for vendor_contract
active before insert position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_vendor_contract, 1);
end;

create or alter trigger bi_contract_detail for contract_detail
active before insert position 0
as
begin
  if (new.id is null) then
    new.id = gen_id(gen_contract_detail, 1);
end;


create or alter trigger ai_contract_detail for contract_detail
active after insert position 0
as
begin
  update Vendor_Contract vc
  set vc.CONTRACT_PRICE = coalesce(
    (select sum(cd.game_price)
    from CONTRACT_DETAIL cd where cd.id_vendor_contract=vc.id), 0)
  where vc.id = new.id_vendor_contract;
end;


create or alter trigger au_contract_detail for contract_detail
active after update position 0
as
begin
  update Vendor_Contract vc
  set vc.CONTRACT_PRICE = coalesce(
    (select sum(cd.game_price)
    from CONTRACT_DETAIL cd where cd.id_vendor_contract=vc.id), 0)
  where vc.id = new.id_vendor_contract;
end;

create or alter trigger au_client_order for client_order
active after update position 0
as
  declare variable v_id_game int;
  declare variable v_game_cnt int;
begin
  if ( (old.is_paid = 0) and (new.is_paid = 1) ) then
    for
      select
        id_game,
        game_cnt
      from ORDER_DETAIL
      where id_client_order = new.id
      into
        :v_id_game,
        :v_game_cnt
    do
      update GAME
      set
        reserved_cnt = reserved_cnt - :v_game_cnt,
        games_in_stock = games_in_stock - :v_game_cnt
      where id = :v_id_game;
end;


create or alter trigger au_vendor_contract for vendor_contract
active after update position 0
as
  declare variable v_id_game int;
  declare variable v_game_cnt int;
begin
  if ( (old.is_received = 0) and (new.is_received = 1) ) then
    for
      select
        id_game,
        game_cnt
      from CONTRACT_DETAIL
      where id_vendor_contract = new.id
      into
        :v_id_game,
        :v_game_cnt
    do
      update GAME
      set games_in_stock = games_in_stock + :v_game_cnt
      where id = :v_id_game;
end;

