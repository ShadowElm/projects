/*insert into CREATOR (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME)
values (1, '����', '��������', '������');

insert into CREATOR (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME)
values (2, '����', '��������', '������');

insert into CREATOR (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME)
values (3, '�������', '����������', '��������');

insert into CREATOR (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME)
values (4, '�������', '����������', '��������');

insert into CREATOR (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME)
values (5, '������', '���������', '�������');

insert into CREATOR (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME)
values (6, '�����', '���������', '�������');

insert into CREATOR (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME)
values (7, '�������', '����������', '��������');

insert into CREATOR (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME)
values (8, 'John', 'D.', 'Green');

insert into CREATOR (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME)
values (9, '��', '����', '���');

insert into CREATOR (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME)
values (10, '������������', '�� ����', '�������');


insert into COUNTRY (ID, CODE_2, COUNTRY)
values (1, 'RU', '������');

insert into COUNTRY (ID, CODE_2, COUNTRY)
values (2, 'CN', '�����');

insert into COUNTRY (ID, CODE_2, COUNTRY)
values (3, 'GB', '��������������');



insert into CLIENT (ID, ID_COUNTRY, FIRST_NAME, MIDDLE_NAME, LAST_NAME, E_MAIL, ADRESS, PHONE_NUMBER)
values (1, 1, '�����', '���������', '�������', 'fedr@mail.ru', '�����-���������, ��-��. ������� 19', '8 999 888 55 44');

insert into CLIENT (ID, ID_COUNTRY, FIRST_NAME, MIDDLE_NAME, LAST_NAME, E_MAIL, ADRESS, PHONE_NUMBER)
values (2, 3, '����', null, '����', 'smit@gmail.com', '������, ������-����� 13', '5555 55 64 34');

insert into CLIENT (ID, ID_COUNTRY, FIRST_NAME, MIDDLE_NAME, LAST_NAME, E_MAIL, ADRESS, PHONE_NUMBER)
values (3, 2, '��', null, '������', 'silun@mail.cn', '�����, ��������� 10', '3 989 458 55 44');



insert into GAME (ID, ID_CREATOR, ID_COUNTRY, NAME, NUMBER_OF_PLAYERS, PRICE, RATING, GAMES_IN_STOCK, RESERVED_CNT)
values (1, 1, 1, '����+', 4, 500.00, 5.6, 7, 0);

insert into GAME (ID, ID_CREATOR, ID_COUNTRY, NAME, NUMBER_OF_PLAYERS, PRICE, RATING, GAMES_IN_STOCK, RESERVED_CNT)
values (2, 2, 1, '�����������', 4, 560.50, 6.7, 1, 0);

insert into GAME (ID, ID_CREATOR, ID_COUNTRY, NAME, NUMBER_OF_PLAYERS, PRICE, RATING, GAMES_IN_STOCK, RESERVED_CNT)
values (3, 3, 1, '��������', 6, 1000.00, 6.86, 3, 0);

insert into GAME (ID, ID_CREATOR, ID_COUNTRY, NAME, NUMBER_OF_PLAYERS, PRICE, RATING, GAMES_IN_STOCK, RESERVED_CNT)
values (4, 4, 1 , '���������',4 , 500.00, 4.0, 12,0);

insert into GAME (ID, ID_CREATOR, ID_COUNTRY, NAME, NUMBER_OF_PLAYERS, PRICE, RATING, GAMES_IN_STOCK, RESERVED_CNT)
values (5, 5, 1 , '�������', 4, 650.00, 6.5, 6, 0);

insert into GAME (ID, ID_CREATOR, ID_COUNTRY, NAME, NUMBER_OF_PLAYERS, PRICE, RATING, GAMES_IN_STOCK, RESERVED_CNT)
values (6, 6, 1 , '��������� �����', 4, 699.99, 7.2, 4, 0);

insert into GAME (ID, ID_CREATOR, ID_COUNTRY, NAME, NUMBER_OF_PLAYERS, PRICE, RATING, GAMES_IN_STOCK, RESERVED_CNT)
values (7, 7, 1 , '������ ����!', 10, 800.00, 7.6, 4, 0);

insert into GAME (ID, ID_CREATOR, ID_COUNTRY, NAME, NUMBER_OF_PLAYERS, PRICE, RATING, GAMES_IN_STOCK, RESERVED_CNT)
values (8, 8, 3, '����� �����', 2, 1200.00, 9.0, 9, 0);

insert into GAME (ID, ID_CREATOR, ID_COUNTRY, NAME, NUMBER_OF_PLAYERS, PRICE, RATING, GAMES_IN_STOCK, RESERVED_CNT)
values (9, 9, 2 , '�������� �����',6 , 1500.00, 9.5, 5, 0);

insert into GAME (ID, ID_CREATOR, ID_COUNTRY, NAME, NUMBER_OF_PLAYERS, PRICE, RATING, GAMES_IN_STOCK, RESERVED_CNT)
values (10, 10, 553 , '������ ���������', 6 , 700.00, 8.3, 5, 0);


insert into GAME_PICTURE (ID, ID_GAME)
values (1, 3);

insert into GAME_PICTURE (ID, ID_GAME)
values (2, 1);


-- ���������� � ���� ������ 0; ����� ���������� ������� ������ ������� �� ������
insert into CLIENT_ORDER (ID, ID_CLIENT, ORDER_DATE, ORDER_PRICE, IS_NEED_DELIVERY, DELIVERY_DATE)
values (1, 1, '15.08.2015', 0, 'N', null);

insert into CLIENT_ORDER (ID, ID_CLIENT, ORDER_DATE, ORDER_PRICE, IS_NEED_DELIVERY, DELIVERY_DATE)
values (2, 2, '07.02.2015', 0, 'Y', '07.03.2015');

insert into CLIENT_ORDER (ID, ID_CLIENT, ORDER_DATE, ORDER_PRICE, IS_NEED_DELIVERY, DELIVERY_DATE)
values (3, 3, '21.09.2015', 0, 'Y', null);


insert into ORDER_DETAIL (ID, ID_CLIENT_ORDER, ID_GAME, GAME_PRICE, GAME_CNT ,IS_PAID,PAID_DATE)
values (1, 1, 1, 500.00, 2, 0, null);

insert into ORDER_DETAIL (ID, ID_CLIENT_ORDER, ID_GAME, GAME_PRICE, GAME_CNT, IS_PAID,PAID_DATE)
values (2, 1, 2, 560.50, 1, 0, null);

insert into ORDER_DETAIL (ID, ID_CLIENT_ORDER, ID_GAME, GAME_PRICE, GAME_CNT, IS_PAID,PAID_DATE)
values (3, 2, 3, 1000.00, 1, 0, null);


--update CLIENT_ORDER co
--set co.ORDER_PRICE = coalesce((select sum(od.game_price) from ORDER_DETAIL od where od.id_client_order=co.id), 0);


insert into VENDOR (ID, ID_COUNTRY, NAME, ADRESS, PHONE_NUMBER)
values (1, 2, 'Hobby World', '�����, ��.������, 5', '3 989 455 74 85');

insert into VENDOR (ID, ID_COUNTRY, NAME, ADRESS, PHONE_NUMBER)
values (2, 1, '������', '�����-���������, ��-� �������, 15', '8 923 945 74 85');

insert into VENDOR (ID, ID_COUNTRY, NAME, ADRESS, PHONE_NUMBER)
values (3, 1, '������', '�.������, ��.�����-������, �.4', '8 923 975 74 89');

insert into VENDOR (ID, ID_COUNTRY, NAME, ADRESS, PHONE_NUMBER)
values (4, 1, '�����', '�.������, ��.��������, �.132', '8 953 945 74 35');

insert into VENDOR (ID, ID_COUNTRY, NAME, ADRESS, PHONE_NUMBER)
values (5, 2, '������', '�������, �����592', '3 989 477 74 85');

insert into VENDOR (ID, ID_COUNTRY, NAME, ADRESS, PHONE_NUMBER)
values (6, 2, '�����', '�����, ������5891', '3 959 455 72 85');

insert into VENDOR (ID, ID_COUNTRY, NAME, ADRESS, PHONE_NUMBER)
values (7, 3, '�������', 'Kent, Folkestone, 7531', '7 288 136 12 45');

insert into VENDOR (ID, ID_COUNTRY, NAME, ADRESS, PHONE_NUMBER)
values (8, 3, '����������', 'London, Folkestone, 3231', '7 278 156 16 65');

insert into VENDOR (ID, ID_COUNTRY, NAME, ADRESS, PHONE_NUMBER)
values (9, 553, '��������', '������, ����� ������, 193', '2 947 118 56 85');

insert into VENDOR (ID, ID_COUNTRY, NAME, ADRESS, PHONE_NUMBER)
values (10, 553, '������', '�������, ��� �����, 96', '2 927 118 39 85');



insert into VENDOR_CONTRACT (ID, ID_VENDOR, CONTRACT_DATE, CONTRACT_PRICE, IS_PAID, PAID_DATE, IS_RECEIVED,
                                 DELIVERY_DATE)
values (1, 1, '07.01.2015', 0, 0, null, 0, null) ;

insert into VENDOR_CONTRACT (ID, ID_VENDOR, CONTRACT_DATE, CONTRACT_PRICE, IS_PAID, PAID_DATE, IS_RECEIVED,
                                 DELIVERY_DATE)
values (2, 2, '05.11.2014', 0, 0, null, 0, null) ;

 insert into VENDOR_CONTRACT (ID, ID_VENDOR, CONTRACT_DATE, CONTRACT_PRICE, IS_PAID, PAID_DATE, IS_RECEIVED,
                                 DELIVERY_DATE)
values (3, 3, '23.11.2013', 0, 0, null, 0, null) ;

insert into VENDOR_CONTRACT (ID, ID_VENDOR, CONTRACT_DATE, CONTRACT_PRICE, IS_PAID, PAID_DATE, IS_RECEIVED,
                                 DELIVERY_DATE)
values (4, 4, '05.05.2014', 0, 0, null, 0, null) ;

insert into VENDOR_CONTRACT (ID, ID_VENDOR, CONTRACT_DATE, CONTRACT_PRICE, IS_PAID, PAID_DATE, IS_RECEIVED,
                                 DELIVERY_DATE)
values (5, 5, '05.11.2014', 0, 0, null, 0, null) ;

insert into VENDOR_CONTRACT (ID, ID_VENDOR, CONTRACT_DATE, CONTRACT_PRICE, IS_PAID, PAID_DATE, IS_RECEIVED,
                                 DELIVERY_DATE)
values (6, 6, '05.03.2015', 0, 0, null, 0, null) ;

insert into VENDOR_CONTRACT (ID, ID_VENDOR, CONTRACT_DATE, CONTRACT_PRICE, IS_PAID, PAID_DATE, IS_RECEIVED,
                                 DELIVERY_DATE)
values (7, 7, '25.09.2014', 0, 0, null, 0, null) ;

insert into VENDOR_CONTRACT (ID, ID_VENDOR, CONTRACT_DATE, CONTRACT_PRICE, IS_PAID, PAID_DATE, IS_RECEIVED,
                                 DELIVERY_DATE)
values (8, 8, '12.12.2014', 0, 0, null, 0, null) ;

insert into VENDOR_CONTRACT (ID, ID_VENDOR, CONTRACT_DATE, CONTRACT_PRICE, IS_PAID, PAID_DATE, IS_RECEIVED,
                                 DELIVERY_DATE)
values (9, 9, '16.07.2014', 0, 0, null, 0, null) ;

insert into VENDOR_CONTRACT (ID, ID_VENDOR, CONTRACT_DATE, CONTRACT_PRICE, IS_PAID, PAID_DATE, IS_RECEIVED,
                                 DELIVERY_DATE)
values (10, 10, '17.07.2014', 0, 0, null, 0, null) ;


insert into CONTRACT_DETAIL (ID, ID_VENDOR_CONTRACT, ID_GAME, GAME_PRICE, GAME_CNT)
values (1, 1, 1, 200, 5) ;

insert into CONTRACT_DETAIL (ID, ID_VENDOR_CONTRACT, ID_GAME, GAME_PRICE, GAME_CNT)
values (2, 1, 2, 300, 2) ;

insert into CONTRACT_DETAIL (ID, ID_VENDOR_CONTRACT, ID_GAME, GAME_PRICE, GAME_CNT)
values (3, 2, 3, 300, 2) ;

insert into CONTRACT_DETAIL (ID, ID_VENDOR_CONTRACT, ID_GAME, GAME_PRICE, GAME_CNT)
values (4, 3, 4, 500, 2) ;

insert into CONTRACT_DETAIL (ID, ID_VENDOR_CONTRACT, ID_GAME, GAME_PRICE, GAME_CNT)
values (5, 4, 5, 250, 1) ;

insert into CONTRACT_DETAIL (ID, ID_VENDOR_CONTRACT, ID_GAME, GAME_PRICE, GAME_CNT)
values (6, 5, 6, 250, 2) ;

insert into CONTRACT_DETAIL (ID, ID_VENDOR_CONTRACT, ID_GAME, GAME_PRICE, GAME_CNT)
values (7, 6, 7, 300, 7) ;

insert into CONTRACT_DETAIL (ID, ID_VENDOR_CONTRACT, ID_GAME, GAME_PRICE, GAME_CNT)
values (8, 7, 8, 400, 2) ;

insert into CONTRACT_DETAIL (ID, ID_VENDOR_CONTRACT, ID_GAME, GAME_PRICE, GAME_CNT)
values (9, 8, 9, 1000, 2) ;

insert into CONTRACT_DETAIL (ID, ID_VENDOR_CONTRACT, ID_GAME, GAME_PRICE, GAME_CNT)
values (10, 9, 9, 900, 4) ;

--update Vendor_Contract vc
--set vc.CONTRACT_PRICE = coalesce((select sum(cd.game_price) from CONTRACT_DETAIL cd where cd.id_vendor_contract=vc.id), 0);
 */

