SET SQL DIALECT 3;

create database 'localhost:/home/data/bitbacket/projects/fb_data/Games_Store.fdb'
user 'SYSDBA' password 'masterkey'
default character set utf8 collation unicode_ci_ai;

create table Creator(
  id int not null primary key,
  first_name varchar(50) not null,                          -- ���
  middle_name varchar(50),                                  -- ��������
  last_name varchar (50) not null                           -- �������
);

create table Country(
  id int not null primary key,
  code_2 varchar(2) not null,                               -- ��� ������
  country varchar (250) not null                            -- ������
);
ALTER TABLE COUNTRY ADD CONSTRAINT UQ_COUNTRY_CODE UNIQUE (CODE_2);

create table Client(
  id int not null primary key,
  id_country int not null references Country(id),           -- ������
  first_name varchar(50) not null,                          -- ���
  middle_name varchar(50),                                  -- ��������
  last_name varchar (50) not null,                          -- �������
  e_mail varchar(100),                                      -- �����
  adress varchar(100) not null,                             -- �����
  phone_number varchar(20) not null                         -- �������
);

create table Game(
  id int not null primary key,
  id_creator int not null references Creator(id),           -- ���������
  id_country int not null references Country(id),           -- ������
  name varchar(100) not null,                               -- �������� ����
  number_of_players int not null,                           -- ���-�� �������
  price numeric(6,2) not null,                              -- ����
  rating numeric(4,2) not null,                             -- �������
  games_in_stock int not null                               -- ���-�� �� ������
);

create table Client_Order(
  id int not null primary key,
  id_client int not null references Game(id),               -- ���. � �������
  order_date date not null,                                 -- ���� ������
  order_price numeric(6,2) not null,                        -- �����
  is_need_delivery char(1) not null,                        -- �����.��������
  delivery_date date                                        -- ���� ��������
);

create table Game_Picture(
  id int not null primary key,
  id_game int not null references Game(id),                 -- �������� ����
  picture_body blob not null                                -- �����������
);

create table Order_Detail(
  id int not null primary key,
  id_order int not null references Client_Order(id),        -- ����� ������
  id_game int not null references Game(id),                 -- ���. �� ����
  game_price numeric(6,2) not null,                         -- ���� ����
  game_cnt int not null,                                    -- ���������� ���
  total_price numeric (6,2)                                 -- �����
);

create table Vendor(
  id int not null primary key,
  id_country int not null references Country(id),           -- ������
  name varchar(100) not null,                               -- ��� ����������
  adress varchar(100) not null,                             -- �����
  phone_number varchar(20) not null                         -- �������
);

create table Vendor_Contract(
  id int not null primary key,
  id_vendor int not null references Vendor(id),             -- ���. � ����������
  contract_date date not null,                              -- ���� ���������
  contract_price numeric(6,2) not null,                     -- �����
  is_paid char(1) not null,                                 -- �������� ��� ���
  paid_date date,                                           -- ���� ������
  is_received char(1) not null,                             -- �������� ��� ���
  delivery_date date                                        -- ���� ���������
);

create table Contract_Detail(
  id int not null primary key,
  id_vendor_contract int not null references Vendor_Contract(id),-- ����� ������
  id_game int not null references Game(id),                      -- ���. �� ����
  game_price numeric(6,2) not null,                              -- ���� ����
  game_cnt int not null,                                         -- ���������� ���
  total_price numeric (6,2)                                      -- �����
);


alter table GAME add reserved_cnt int not null;

alter table Client_Order add is_paid char(1) not null;

alter table Client_Order add paid_date date;

-- alter table Order_detail drop constraint integ_41;
-- alter table Order_detail alter id_order to id_client_order;
-- alter table Order_detail add CONSTRAINT integ_41 FOREIGN KEY(id_client_order) REFERENCES client_order (id);

-- alter table client_order add order_num varchar(20);
-- alter table vendor_contract add contract_num varchar(20);

-- update client_order set  order_num = gen_id(GEN_CLIENT_ORDER_NUM,1);
-- update vendor_contract set  contract_num = gen_id(GEN_VENDOR_CONTRACT_NUM,1);

--alter table order_detail drop total_price;
--alter table contract_detail drop total_price;
