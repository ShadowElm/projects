create table Vendor(
  id int not null primary key,
  id_country int not null references Country(id),           -- ������
  name varchar(100) not null,                               -- ��� ����������
  adress varchar(100) not null,                             -- �����
  phone_number varchar(20) not null                         -- �������
);

create table Vendor_Contract(
  id int not null primary key,
  id_vendor int not null references Vendor(id),             -- ���. � ����������
  contract_date date not null,                              -- ���� ���������
  contract_price numeric(6,2) not null,                     -- �����
  is_paid char(1) not null,                                 -- �������� ��� ���
  paid_date date,                                           -- ���� ������
  is_received char(1) not null,                             -- �������� ��� ���
  delivery_date date                                        -- ���� ���������
);

create table Contract_Detail(
  id int not null primary key,
  id_vendor_contract int not null references Vendor_Contract(id),-- ����� ������
  id_game int not null references Game(id),                      -- ���. �� ����
  game_price numeric(6,2) not null,                              -- ���� ����
  game_cnt int not null,                                         -- ���������� ���
  total_price numeric (6,2)                                      -- �����
);


alter table GAME add reserved_cnt int not null;      -- ����, ����������� � ������, �� ��� �� ���������

alter table Client_Order add is_paid char(1) not null; -- ���� ������ ������

alter table Client_Order add paid_date date;            -- ���� ������

alter table Client_Order add is_need_delivery char(1) not null; -- ���� ������������� ��������

alter table Client_Order add delivery_date date;            -- ���� ��������� ������

