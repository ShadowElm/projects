-- Добавим запись в таблицу клиентов
create procedure insert_client (id integer, id_country integer, FIRST_NAME varchar(50),
 MIDDLE_NAME varchar(50), LAST_NAME varchar(50), E_MAIL varchar(100), ADRESS varchar(100), PHONE_NUMBER varchar(20))
as
begin
    insert into CLIENT (ID, ID_COUNTRY, FIRST_NAME, MIDDLE_NAME, LAST_NAME, E_MAIL, ADRESS, PHONE_NUMBER)
    values (:ID, :ID_COUNTRY, :FIRST_NAME, :MIDDLE_NAME, :LAST_NAME, :E_MAIL, :ADRESS, :PHONE_NUMBER);
end;

-- Добавим запись в таблицу заказов
create procedure insert_client_order (ID integer, ID_CLIENT integer, ORDER_NUM varchar(20),
 ORDER_DATE date, ORDER_PRICE numeric(6,2), IS_NEED_DELIVERY char(1), DELIVERY_DATE date,
  IS_PAID char (1), PAID_DATE date)
as
begin
insert into CLIENT_ORDER (ID, ID_CLIENT, ORDER_NUM, ORDER_DATE, ORDER_PRICE, IS_NEED_DELIVERY, DELIVERY_DATE, IS_PAID,
                          PAID_DATE)
values (:ID, :ID_CLIENT, :ORDER_NUM, :ORDER_DATE, :ORDER_PRICE, :IS_NEED_DELIVERY, :DELIVERY_DATE, :IS_PAID, :PAID_DATE);
end;

-- Добавим запись в таблицу деталей контрактов
create procedure insert_contract_detail (ID integer, ID_VENDOR_CONTRACT integer,
 ID_GAME integer, GAME_PRICE numeric(6,2), GAME_CNT integer)
as
begin
 insert into CONTRACT_DETAIL (ID, ID_VENDOR_CONTRACT, ID_GAME, GAME_PRICE, GAME_CNT)
 values (:ID, :ID_VENDOR_CONTRACT, :ID_GAME, :GAME_PRICE, :GAME_CNT);
end;

-- Добавим запись в таблицу стран
create procedure insert_country (ID integer, CODE_2 varchar(2), NAME varchar(250))
as
begin
 insert into COUNTRY (ID, CODE_2, NAME)
 values (:ID, :CODE_2, :NAME);
end;

-- Добавим запись в таблицу создателей
create procedure insert_creator (ID integer, FIRST_NAME varchar(50),
 MIDDLE_NAME varchar(50), LAST_NAME varchar(50))
as
begin
  insert into CREATOR (ID, FIRST_NAME, MIDDLE_NAME, LAST_NAME)
  values (:ID, :FIRST_NAME, :MIDDLE_NAME, :LAST_NAME);
end;

-- Добавим запись в таблицу игр
create procedure insert_game (ID integer, ID_CREATOR integer, ID_COUNTRY integer,
 NAME varchar(100), NUMBER_OF_PLAYERS integer, PRICE numeric (6,2), RATING numeric (6,2),
  GAMES_IN_STOCK integer, RESERVED_CNT integer)
as
begin
 insert into GAME (ID, ID_CREATOR, ID_COUNTRY, NAME, NUMBER_OF_PLAYERS, PRICE, RATING, GAMES_IN_STOCK, RESERVED_CNT)
 values (:ID, :ID_CREATOR, :ID_COUNTRY, :NAME, :NUMBER_OF_PLAYERS, :PRICE, :RATING, :GAMES_IN_STOCK, :RESERVED_CNT);
end;

-- Добавим запись в таблицу деталей заказа
create procedure insert_order_detail (ID integer, ID_CLIENT_ORDER integer,
 ID_GAME integer, GAME_PRICE numeric(6,2), GAME_CNT integer)
as
begin
  insert into ORDER_DETAIL (ID, ID_CLIENT_ORDER, ID_GAME, GAME_PRICE, GAME_CNT)
  values (:ID, :ID_CLIENT_ORDER, :ID_GAME, :GAME_PRICE, :GAME_CNT) ;
end;

-- Добавим запись в таблицу издателей
create procedure insert_vendor(ID integer, ID_COUNTRY integer, NAME varchar(100),
 ADRESS varchar(100), PHONE_NUMBER varchar(20))
as
begin
insert into VENDOR (ID, ID_COUNTRY, NAME, ADRESS, PHONE_NUMBER)
values (:ID, :ID_COUNTRY, :NAME, :ADRESS, :PHONE_NUMBER)  ;
end;

-- Добавим запись в таблицу контрактов
create procedure insert_vendor_contract (ID integer, ID_VENDOR integer,
 CONTRACT_NUM varchar(20), CONTRACT_DATE date, CONTRACT_PRICE numeric(6,2),
  IS_PAID char(1), PAID_DATE date,IS_RECEIVED char(1), DELIVERY_DATE date)
as
begin
 insert into VENDOR_CONTRACT (ID, ID_VENDOR, CONTRACT_NUM, CONTRACT_DATE, CONTRACT_PRICE, IS_PAID, PAID_DATE,
                              IS_RECEIVED, DELIVERY_DATE)
 values (:ID, :ID_VENDOR, :CONTRACT_NUM, :CONTRACT_DATE, :CONTRACT_PRICE, :IS_PAID, :PAID_DATE, :IS_RECEIVED,
         :DELIVERY_DATE) ;
end;

-- Повысим цену игр, которые стоят дешевле 500.00
create procedure update_game_price(add_s numeric(6,2))
as
begin
  update GAME
  set PRICE = :ADD_S+price
      where (GAME.Price < 500.00);
end;

-- Удалим из заказов те игры, которые стоят дороже всего
create procedure delete_game(id integer)
as
begin
   delete from ORDER_DETAIL od
   where od.ID_CLIENT_ORDER = :ID and od.GAME_PRICE = (select max(od.GAME_PRICE) from ORDER_Detail od  where od.ID_CLIENT_ORDER = :ID);
end;

-- Удалим те игры, которые никто не заказал
create procedure del_game
as
begin
   delete from GAME
   where ID not in (select od.ID_GAME from order_detail od);
end;

-- Удалим тех создателей, чьих игр нет в магазине
create procedure
as
begin
delete from creator where id not in (selsect g.ID_CREATOR from game g);
end;
