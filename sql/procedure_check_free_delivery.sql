create or alter procedure CHECK_FREE_DELIVERY (
    NEW_ORDER_PRICE numeric(6,2))
returns (
    IS_FREE_DELIVERY char(1))
AS
  declare variable ORDER_PRICE_FOR_FREE_DELIVERY numeric(6,2);
begin
  ORDER_PRICE_FOR_FREE_DELIVERY = 1500.00;
  if (new_order_price>=ORDER_PRICE_FOR_FREE_DELIVERY) then is_free_delivery='1';
  else is_free_delivery='0';
end

